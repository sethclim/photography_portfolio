import React from "react";

function PhotoObject({ img }) {
  return (
    <div className="photoobject">
      <img src={img} alt="photos"></img>
    </div>
  );
}

export default PhotoObject;
