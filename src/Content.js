import React from "react";
import PhotoSlider from "./PhotoSlider";

function Content() {
  return (
    <div className="main-content">
      <PhotoSlider />
    </div>
  );
}

export default Content;
