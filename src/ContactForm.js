import React from "react";

class ContactForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      email: "",
      message: "",
    };
  }
  render() {
    return (
      <form
        id="contact-form"
        className="contact-form"
        //onSubmit={this.handleSubmit.bind(this)}
        //method="POST"
      >
        <div className="form-row">
          <label className="label" htmlFor="name">
            Name
          </label>
        </div>
        <div className="form-row">
          <input
            type="text"
            className="form-control-input"
            value={this.state.name}
            //onChange={this.onNameChange.bind(this)}
          />
        </div>

        <div className="form-row">
          <label className="label" htmlFor="exampleInputEmail1">
            Email address
          </label>
        </div>

        <div className="form-row">
          <input
            type="email"
            className="form-control-input"
            aria-describedby="emailHelp"
            value={this.state.email}
            //onChange={this.onEmailChange.bind(this)}
          />
        </div>
        <div className="form-row">
          <label className="label" htmlFor="message">
            Message
          </label>
        </div>

        <div className="form-row">
          <textarea
            className="form-control-textera"
            rows="5"
            value={this.state.message}
            //onChange={this.onMessageChange.bind(this)}
          />
        </div>
        <div className="form-row">
          <button type="submit" className="submit-btn">
            Submit
          </button>
        </div>
      </form>
    );
  }
}

export default ContactForm;
