import React, { PureComponent } from "react";
import Arrow from "./Arrow";

class Controls extends PureComponent {
  state = {
    showBox: false,
  };

  handleBoxEnter = () => this.setState({ showBox: true });
  handleBoxLeave = () => this.setState({ showBox: false });
  render() {
    let fillLeft = "#333333";
    let fillRight = "#333333";
    if (!this.state.showBox) {
      fillLeft = "#d9d9d9";
      fillRight = "#d9d9d9";
    }

    if (!this.props.left) {
      fillLeft = "#bfbfbf";
    }

    if (!this.props.right) {
      fillRight = "#bfbfbf";
    }

    return (
      <div className="controls_container">
        <div
          onMouseEnter={this.handleBoxEnter}
          onMouseLeave={this.handleBoxLeave}
          className={`${
            this.state.showBox ? " controls" : "controls_deactivated"
          }`}
        >
          <div className="controls-back">
            <div
              type="image"
              alt="back"
              className="back_btn"
              disabled={!this.props.left}
              onClick={() => {
                this.props.scroll("+=-400");
              }}
            >
              <Arrow className="arrow" fill={fillLeft} />
            </div>
          </div>
          <div className="controls-forward">
            <div
              type="image"
              alt="forward"
              className="forward_btn"
              disabled={!this.props.right}
              onClick={() => {
                this.props.scroll("+=400");
              }}
            >
              <Arrow className="arrow" fill={fillRight} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Controls;
