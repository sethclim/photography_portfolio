import React from "react";
import ContactForm from "./ContactForm";
import andrewsterling from "./Photos/andrew-sterling.jpg";

function Contact() {
  return (
    <div className="main-content">
      <div className="aboutwrap">
        <div className="imageSide">
          <div>
            <img className="Photo" src={andrewsterling} alt="photos"></img>
          </div>
        </div>
        <div className="formSide">
          <div className="formContainer">
            <div className="form-title">
              <h1>Send us an Email</h1>
            </div>
            <ContactForm />
          </div>
        </div>
      </div>
    </div>
  );
}

export default Contact;
