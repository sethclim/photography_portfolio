import React from "react";
import { Link } from "react-router-dom";

function Header() {
  return (
    <div className="main-header">
      <div className="container">
        <h3>Sardasky's Photofolio</h3>
        <nav className="main-nav">
          <ul className="main-nav-list">
            <li>
              <Link to="/">Portfolio</Link>
            </li>
            <li>
              <Link to="/about">About Us</Link>
            </li>

            <li>
              <Link to="/contact">Contact</Link>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  );
}

export default Header;
