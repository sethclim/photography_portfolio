import React from "react";
import lachlan from "./Photos/lachlan-gowen.jpg";

function AboutUs() {
  return (
    <div className="main-content">
      <div className="aboutwrap">
        <div className="imageSide">
          <div>
            <img className="Photo" src={lachlan} alt="photos"></img>
          </div>
        </div>
        <div className="textSide">
          <div className="textWrap">
            <p>
              My name is Sardasky and I take pictures of Nature. I grew up in
              eastern Europe and now call the American Mid-West home. When I'm
              not traversing the country taking epic photo's, I'm at home
              watching the fire with my dog, Brono.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AboutUs;
