import React, { PureComponent } from "react";
import debounce from "lodash.debounce";
import PhotoObject from "./PhotoObject";
import Andrew from "./Photos/andrew-sterling.jpg";
import Tovar from "./Photos/gabriel-tovar.jpg";
import Nicolas from "./Photos/nicolas-j-leclercq.jpg";
import Controls from "./Controls";

import $ from "jquery";

export default class PhotoSlider extends PureComponent {
  constructor() {
    super();
    this.state = {
      hasOverflow: false,
      canScrollLeft: false,
      canScrollRight: false,
    };

    this.checkForOverflow = this.checkForOverflow.bind(this);
    this.checkForScrollPosition = this.checkForScrollPosition.bind(this);

    this.debounceCheckForOverflow = debounce(this.checkForOverflow, 1000);
    this.debounceCheckForScrollPosition = debounce(
      this.checkForScrollPosition,
      200
    );

    this.container = null;
  }

  componentDidMount() {
    this.checkForOverflow();
    this.checkForScrollPosition();

    this.container.addEventListener(
      "scroll",
      this.debounceCheckForScrollPosition
    );
    console.log(
      "componentDidMount scroll left, scrollWidth, clientWidth",
      this.container.scrollLeft,
      this.container.scrollWidth,
      this.container.clientWidth
    );
  }

  componentWillUnmount() {
    this.container.removeEventListener(
      "scroll",
      this.debounceCheckForScrollPosition
    );
    this.debounceCheckForOverflow.cancel();
  }

  checkForScrollPosition() {
    const { scrollLeft, scrollWidth, clientWidth } = this.container;

    this.setState({
      canScrollLeft: scrollLeft > 0,
      canScrollRight: scrollLeft !== scrollWidth - clientWidth,
    });
  }

  checkForOverflow() {
    const { scrollWidth, clientWidth } = this.container;
    const hasOverflow = scrollWidth > clientWidth;

    this.setState({ hasOverflow });
  }

  scrollContainerBy(distance) {
    console.log("scroll Container called");
    if (document.querySelector(".photoslider") != null) {
      $(".photoslider").animate({ scrollLeft: distance }, 800);
    } else {
      console.log("container is null");
    }
  }

  buildControls() {
    const { canScrollLeft, canScrollRight } = this.state;
    return (
      <Controls
        left={canScrollLeft}
        right={canScrollRight}
        scroll={this.scrollContainerBy}
      />
    );
  }

  render() {
    return (
      <>
        <div
          className="photoslider"
          ref={(node) => {
            this.container = node;
          }}
        >
          <PhotoObject img={Andrew} />
          <PhotoObject img={Tovar} />
          <PhotoObject img={Nicolas} />
          <PhotoObject img={Andrew} />
          <PhotoObject img={Andrew} />
          <PhotoObject img={Andrew} />
        </div>
        <div>{this.buildControls()}</div>
      </>
    );
  }
}
