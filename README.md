# Photofolio
A React Website for a photographer.  Includes a sliding gallery using react and jQuery, as well as interactive controls for viewing.

## Run Project

In the project directory,run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

## Technologies Used

-**React.JS** -**JQuery** -**JSX** -**CSS** 

## Demo
![Photofolio.png](https://bitbucket.org/repo/gyo6goe/images/2022217767-Photofolio.png)

## Usage
Use the responive controls to scroll left and right through the gallery of pictures.

## Key Project Lessons
* Building an animated scroll for the gallery
* Creating intelligent controls that know when the gallery is at the end or the beginning.
* Using JQuery

## Maintainers
This project is mantained by:
* [Seth Climenhaga](https://bitbucket.org/sethclim/) 
